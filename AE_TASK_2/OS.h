/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/
                          
#ifndef _OS_INT_H                   
#define _OS_INT_H                   
               
#include "OS_cfg.h"		

/*************************************/
/* Description : This function is    */
/* used to initalize OS              */
/*************************************/
extern void OS_init(void);


/*************************************/
/* Description : This function is    */
/* used to start OS                  */
/*************************************/
extern void OS_start(void);


#endif
