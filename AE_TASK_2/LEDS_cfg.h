/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/

#ifndef _LEDS_config_H
#define _LEDS_config_H


#define LEDS_u8_MODE_NORMAL 1
#define LEDS_u8_MODE_REVERSE 0

#define LEDS_u8_STATE_LOW 0
#define LEDS_u8_STATE_HIGH 1


typedef struct{
  u8 led_mode;
  u8 led_pin;
  u8 led_init_state;
}Led;


extern const u8 led_max;
extern const Led leds[];

#endif 
