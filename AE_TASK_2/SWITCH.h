/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/
                                     
#ifndef _SWITCH_INT_H                   
#define _SWITCH_INT_H                   
               
#include "SWITCH_cfg.h"			   

				    
/*************************************/
/* Description : This function is    */
/* used to get switch state either   */
/* pushed or released                */
/*************************************/
extern u8 SWITCH_u8GetSwitchState(u8 u8SwitchIndex);







#endif
