/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 7 FEB 2018              */
/* Release : VOL                     */
/*************************************/
#ifndef _GIE_INT_H
#define _GIE_INT_H


/*************************************/
/* Description : This function is    */
/* used to enable global interrupt   */
/*************************************/
extern void GIE_voidEnableGlobalInterrupt(void);


/*************************************/
/* Description : This function is    */
/* used to disable global interrupt  */
/*************************************/
extern void GIE_voidDisableGlobalInterrupt(void);





#endif