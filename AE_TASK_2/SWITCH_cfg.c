/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/

#include "STD_TYPES.h"
#include "DIO.h"
#include "SWITCH_cfg.h"


const Switch switches[] = {
        {DIO_u8_PIN10,DIO_u8_LOW},
        {DIO_u8_PIN11,DIO_u8_LOW}
};

const u8 switch_max = sizeof(switches)/sizeof(Switch);


