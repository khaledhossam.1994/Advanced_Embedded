/*
 * Keypad_handler.c
 *
 *  Created on: Mar 7, 2018
 *      Author: admin
 */


#include "STD_TYPES.h"
#include "BIT_MATH.h"
#include "Clock.h"
#include "KEYPAD.h"
#include "Keypad_handler.h"




s8 keypad_input_number = 0;



void Keypad_Input(void)
{

	for(u8 i = 0;i<16;i++)
	{
		if(GET_BIT(KEYPAD_u16GetKeyPadState(),i))
		{
			if(3 == i)
			{
				keypad_input_number = -1;
				seconds_flag = 1;
				minutes_flag = 0;
				hours_flag = 0;
			}else if(7 == i)
			{
				keypad_input_number = -1;
				seconds_flag = 0;
				minutes_flag = 1;
				hours_flag = 0;
			}else if(11 == i)
			{
				keypad_input_number = -1;
				seconds_flag = 0;
				minutes_flag = 0;
				hours_flag = 1;
			}else if(12 == i && !adjust_minutes_flag && !adjust_hours_flag)
			{
				keypad_input_number = -1;
				adjust_seconds_flag = 1;
			}else if(14 == i && !adjust_seconds_flag && !adjust_hours_flag)
			{
				keypad_input_number = -1;
				adjust_minutes_flag = 1;
			}else if(15 == i && !adjust_minutes_flag && !adjust_seconds_flag)
			{
				keypad_input_number = -1;
				adjust_hours_flag = 1;
			}
			else
			{
				if(0 == i) { keypad_input_number = 1; }
				else if(1 == i) { keypad_input_number = 2; }
				else if(2 == i) { keypad_input_number = 3; }
				else if(4 == i) { keypad_input_number = 4; }
				else if(5 == i) { keypad_input_number = 5; }
				else if(6 == i) { keypad_input_number = 6; }
				else if(8 == i) { keypad_input_number = 7; }
				else if(9 == i) { keypad_input_number = 8; }
				else if(10 == i) { keypad_input_number = 9; }
				else if(13 == i) { keypad_input_number = 0; }
			}
			break;
		}
		else
		{
			keypad_input_number = -1;
		}
	}

}
