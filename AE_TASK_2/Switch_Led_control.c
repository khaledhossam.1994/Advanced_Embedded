/*
 * Switch_Led_control.c
 *
 *  Created on: Mar 7, 2018
 *      Author: admin
 */


#include "STD_TYPES.h"
#include "BIT_MATH.h"
#include "SWITCH.h"
#include "LEDS.h"
#include "Switch_Led_control.h"





void Switch_Led_1(void)
{
	//LEDS_voidSetLedOn(0);
	if(SWITCH_u8GetSwitchState(0) == SWITCH_PUSHED_STATE)
	{
		LEDS_voidSetLedOn(0);
	}
	else
	{
		LEDS_voidSetLedOff(0);
	}
}


void Switch_Led_2(void)
{
	//LEDS_voidSetLedOn(1);
	if(SWITCH_u8GetSwitchState(1) == SWITCH_PUSHED_STATE)
	{
		LEDS_voidSetLedOn(1);
	}
	else
	{
		LEDS_voidSetLedOff(1);
	}
}
