/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 7 MAR 2018              */
/* Release : VOL  1                  */
/*************************************/

#ifndef _TIM_CONFIG_H
#define _TIM_CONFIG_H


#define TIFR  	*((volatile u8*) 0x58)
#define TIMSK   *((volatile u8*) 0x59)
#define TCCR0   *((volatile u8*) 0x53)
#define TCNT0   *((volatile u8*) 0x52)
#define OCR0    *((volatile u8*) 0x5C)


#define TIM_u8_PRESCALER_NO_DIV 	0b00001001
#define TIM_u8_PRESCALER_8 		    0b00001010
#define TIM_u8_PRESCALER_64 	    0b00001011
#define TIM_u8_PRESCALER_256 	    0b00001100
#define TIM_u8_PRESCALER_1024 	  	0b00001101


#define  TIM_u16_NO_DIVISION  1
#define  TIM_u16_DIV_BY_8     8
#define  TIM_u16_DIV_BY_64    64
#define  TIM_u16_DIV_BY_256   256
#define  TIM_u16_DIV_BY_1024  1024


/* Description: Prescaler         */
/* Range      : TIM_u16_NO_DIVISION */
/*              TIM_u16_DIV_BY_8    */
/*              TIM_u16_DIV_BY_64   */
/*              TIM_u16_DIV_BY_256  */
/*              TIM_u16_DIV_BY_1024 */
#define TIM_u16_PRESCALER TIM_u16_DIV_BY_64

/* Description: Timer initial value */
#define TIM_u8_COMPARE_REGSITER_VALUE 124


#endif


