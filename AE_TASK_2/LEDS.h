/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/

#ifndef _LEDS_INT_H
#define _LEDS_INT_H


#include "LEDS_cfg.h"

extern void LEDS_voidInitialize(void);
extern void LEDS_voidSetLedOn(u8 u8LedPinIndex);
extern void LEDS_voidSetLedOff(u8 u8LedPinIndex);

#endif
