#ifndef _DIO_CONFIG_H
#define _DIO_CONFIG_H


#define DIO_u8_INPUT  0
#define DIO_u8_OUTPUT 1

#define DIO_u8_DIR_PIN0    DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN1    DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN2    DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN3    DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN4    DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN5    DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN6    DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN7    DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN8    DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN9    DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN10   DIO_u8_INPUT
#define DIO_u8_DIR_PIN11   DIO_u8_INPUT
#define DIO_u8_DIR_PIN12   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN13   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN14   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN15   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN16   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN17   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN18   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN19   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN20   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN21   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN22   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN23   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN24   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN25   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN26   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN27   DIO_u8_OUTPUT
#define DIO_u8_DIR_PIN28   DIO_u8_INPUT
#define DIO_u8_DIR_PIN29   DIO_u8_INPUT
#define DIO_u8_DIR_PIN30   DIO_u8_INPUT
#define DIO_u8_DIR_PIN31   DIO_u8_INPUT





#endif
