/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/

#ifndef _LED_config_H
#define _LED_config_H


/*7 segment cathode and anode values*/
/*Common cathode value*/
#define SSEGMENT_COMMON_CATHODE 0
/*Common anode value*/
#define SSEGMENT_COMMON_ANODE 	1


/*SSEGMENT PINs indices*/

/*PIN A index*/
#define SSEGMENT_PIN_A_INDEX	0
/*PIN B index*/
#define SSEGMENT_PIN_B_INDEX	1
/*PIN C index*/
#define SSEGMENT_PIN_C_INDEX	2
/*PIN D index*/
#define SSEGMENT_PIN_D_INDEX	3
/*PIN E index*/
#define SSEGMENT_PIN_E_INDEX	4
/*PIN F index*/
#define SSEGMENT_PIN_F_INDEX	5
/*PIN G index*/
#define SSEGMENT_PIN_G_INDEX	6
/*PIN DOT index*/
#define SSEGMENT_PIN_DOT_INDEX 	7



typedef struct{
  u8 ss_type;
  u8 ssegment_pin_A;
  u8 ssegment_pin_B;
  u8 ssegment_pin_C;
  u8 ssegment_pin_D;
  u8 ssegment_pin_E;
  u8 ssegment_pin_F;
  u8 ssegment_pin_G;
  u8 ssegment_pin_DOT;
}Segment;

extern const Segment segments[];
extern const u8 u8SegmentValues[16];
extern const u8 segment_max;

#endif 