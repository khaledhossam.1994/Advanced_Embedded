/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/

#include "STD_TYPES.h"
#include "BIT_MATH.h"


#include "OS_cfg.h"


static u8 flag = 0;
static u32 Ticks = 0;

/*************************************/
/* Description : This function is    */
/* used to initalize OS              */
/*************************************/
void OS_init(void)
{
  
}


/*************************************/
/* Description : This function is    */
/* used to start OS                  */
/*************************************/
void OS_start(void)
{
  static u8 i = 0;
  while(1)
  {
    if(1 == flag)
    {
      flag = 0;
      for(i = 0; i<tasks_max; i++)
      {
        if(Ticks % os_Tasks[i].periodicity == 0)
        {
          os_Tasks[i].task();
        }
      }
    }
  }
}



void __vector_10 (void) __attribute__((signal,used));
void __vector_10 (void)
{
	flag = 1;
	Ticks++;
}






