#ifndef _BIT_MATH_H
#define _BIT_MATH_H



//********** Bits Manipulation Macros ***************************

#define SET_BIT(reg,bitN)    ((reg) |= (1<<bitN))        // Sets a bit (bitN) in a register (reg)
#define CLEAR_BIT(reg,bitN)  ((reg) &= ~(1<<bitN))       // Clears a bit (bitN) in a register (reg)
#define TOGGLE_BIT(reg,bitN) ((reg) ^= (1<<bitN))        // Toggles a bit (bitN) in a register (reg)
#define GET_BIT(reg,bitN)    (1 & ((reg)>>bitN))         // Returns value of bit (bitN) in a register (reg)
#define SHIFT_CRIGHT(reg,x) do{ for(int i=0;i<x;i++) {\
							reg = ((reg&1) << (sizeof(reg)*8)-1) | (reg>>1); }\
						}while(0)                       // Shifts the register (reg) circular right by value (x)  
							
#define SHIFT_CLEFT(reg,x) do{ for(int i=0;i<x;i++) {\
							reg = ((reg & (1 << (sizeof(reg)*8)-1)) >> (sizeof(reg)*8)-1)   | (reg<<1); }\
						}while(0)	                    // Shifts the register (reg) circular left by value (x)						
							
						
			
#define BIT_CONC(B7,B6,B5,B4,B3,B2,B1,B0) CONC_HELP(B7,B6,B5,B4,B3,B2,B1,B0)
#define CONC_HELP(B7,B6,B5,B4,B3,B2,B1,B0) 0b##B7##B6##B5##B4##B3##B2##B1##B0

			
#endif
