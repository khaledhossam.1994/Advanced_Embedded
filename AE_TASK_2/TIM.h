/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 7 MAR 2018              */
/* Release : VOL  1                  */
/*************************************/
#ifndef _TIM_INT_H
#define _TIM_INT_H


#include "TIM_cfg.h"



/*************************************/
/* Description : This function is    */
/* used to initalize the timer       */
/* peripheral                        */
/*************************************/
extern void TIM_voidInitialize(void);


/*************************************/
/* Description : This function is    */
/* used to enable Timer interrupt    */
/*************************************/
extern void TIM_voidEnableInt(void);


/*************************************/
/* Description : This function is    */
/* used to disable Timer interrupt   */
/*************************************/
extern void TIM_voidDisableInt(void);


/*************************************/
/* Description : This function is    */
/* used to initialize timer register */
/*************************************/
extern void TIM_voidInitRegister(u8 u8ValueCpy);



#endif
