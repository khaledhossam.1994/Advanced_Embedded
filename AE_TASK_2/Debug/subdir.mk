################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Clock.c \
../DIO_prog.c \
../GIE_prog.c \
../KEYPAD.c \
../KEYPAD_cfg.c \
../Keypad_handler.c \
../LEDS.c \
../LEDS_cfg.c \
../OS.c \
../OS_cfg.c \
../SSEGMENT.c \
../SSEGMENT_cfg.c \
../SWITCH.c \
../SWITCH_cfg.c \
../Switch_Led_control.c \
../TIM.c \
../TIM_cfg.c \
../delay.c \
../main.c 

OBJS += \
./Clock.o \
./DIO_prog.o \
./GIE_prog.o \
./KEYPAD.o \
./KEYPAD_cfg.o \
./Keypad_handler.o \
./LEDS.o \
./LEDS_cfg.o \
./OS.o \
./OS_cfg.o \
./SSEGMENT.o \
./SSEGMENT_cfg.o \
./SWITCH.o \
./SWITCH_cfg.o \
./Switch_Led_control.o \
./TIM.o \
./TIM_cfg.o \
./delay.o \
./main.o 

C_DEPS += \
./Clock.d \
./DIO_prog.d \
./GIE_prog.d \
./KEYPAD.d \
./KEYPAD_cfg.d \
./Keypad_handler.d \
./LEDS.d \
./LEDS_cfg.d \
./OS.d \
./OS_cfg.d \
./SSEGMENT.d \
./SSEGMENT_cfg.d \
./SWITCH.d \
./SWITCH_cfg.d \
./Switch_Led_control.d \
./TIM.d \
./TIM_cfg.d \
./delay.d \
./main.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -O0 -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega32 -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


