/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/
                          
#ifndef _KEYPAD_INT_H                   
#define _KEYPAD_INT_H                   
               
#include "KEYPAD_cfg.h"		

	   
/*************************************/
/* Description : This function is    */
/* used to get keypad status , and   */
/* each switch has a dedicated bit   */
/*************************************/
extern u16 KEYPAD_u16GetKeyPadState(void);


/*************************************/
/* Description : This function is    */
/* used to get keypad status , and   */
/* each switch has a dedicated bit   */
/*************************************/
extern void voidSetSwitchResult(u8 u8Column);


#endif
