/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 7 FEB 2018              */
/* Release : VOL                     */
/*************************************/
#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "GIE_register.h"
#include "GIE_config.h"
#include "GIE_priv.h"
#include "GIE_int.h"




/*************************************/
/* Description : This function is    */
/* used to enable global interrupt   */
/*************************************/
extern void GIE_voidEnableGlobalInterrupt(void)
{
	SET_BIT(SREG,7);
}


/*************************************/
/* Description : This function is    */
/* used to disable global interrupt  */
/*************************************/
extern void GIE_voidDisableGlobalInterrupt(void)
{
		CLEAR_BIT(SREG,7);
}