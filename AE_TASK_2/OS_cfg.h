/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/
          
#ifndef _OS_CONFIG_H                   
#define _OS_CONFIG_H          




typedef struct{
  void (*task) (void);
  u16 periodicity;
}os_task;


extern const os_task os_Tasks[];
extern const u16 tasks_max;

#endif
