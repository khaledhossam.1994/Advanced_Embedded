/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/

#include "STD_TYPES.h"
#include "BIT_MATH.h"


#include "DIO.h"
#include "KEYPAD.h"



static u16 u16ResultLoc = 0;

                                                
/*************************************/
/* Description : This function is    */
/* used to get keypad status , and   */
/* each switch has a dedicated bit   */
/*************************************/
u16 KEYPAD_u16GetKeyPadState(void)
{
	/* Clear previous status */
	u16ResultLoc = 0;
	
	/* Phase 1 */
	DIO_voidSetPinValue(KEYPAD_u8_OUT_1,DIO_u8_LOW);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_2,DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_3,DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_4,DIO_u8_HIGH);
	voidSetSwitchResult(0);

	/* Phase 2 */
	DIO_voidSetPinValue(KEYPAD_u8_OUT_1,DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_2,DIO_u8_LOW);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_3,DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_4,DIO_u8_HIGH);
	voidSetSwitchResult(1);

	/* Phase 3 */
	DIO_voidSetPinValue(KEYPAD_u8_OUT_1,DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_2,DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_3,DIO_u8_LOW);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_4,DIO_u8_HIGH);
	voidSetSwitchResult(2);
	
	/* Phase 4 */
	DIO_voidSetPinValue(KEYPAD_u8_OUT_1,DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_2,DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_3,DIO_u8_HIGH);
	DIO_voidSetPinValue(KEYPAD_u8_OUT_4,DIO_u8_LOW);
	voidSetSwitchResult(3);
	
	return u16ResultLoc;
}



/*************************************/
/* Description : This function is    */
/* used to get keypad status , and   */
/* each switch has a dedicated bit   */
/*************************************/
void voidSetSwitchResult(u8 u8Column)
{
	
	if(DIO_u8GetPinValue(KEYPAD_u8_INP_1) == DIO_u8_LOW)
	{
		SET_BIT(u16ResultLoc,u8Column+0);
	}
	
	if(DIO_u8GetPinValue(KEYPAD_u8_INP_2) == DIO_u8_LOW)
	{
		SET_BIT(u16ResultLoc,u8Column+4);
	}
	
	if(DIO_u8GetPinValue(KEYPAD_u8_INP_3) == DIO_u8_LOW)
	{
		SET_BIT(u16ResultLoc,u8Column+8);
	}
	
	if(DIO_u8GetPinValue(KEYPAD_u8_INP_4) == DIO_u8_LOW)
	{
		SET_BIT(u16ResultLoc,u8Column+12);
	}
}






