/*
 * Clock.c
 *
 *  Created on: Mar 7, 2018
 *      Author: admin
 */


#include "STD_TYPES.h"
#include "BIT_MATH.h"
#include "Clock.h"
#include "SSEGMENT.h"
#include "Keypad_handler.h"


static u8 clock_stop = 0;
static u8 adjust_numbers_count = 0;
static u8 seconds = 0;
static u8 minutes = 0;
static u8 hours = 0;

u8 seconds_flag = 1;
u8 minutes_flag = 0;
u8 hours_flag = 0;

u8 adjust_seconds_flag = 0;
u8 adjust_minutes_flag = 0;
u8 adjust_hours_flag = 0;

u8 adjust_seconds_window = 0;
u8 adjust_minutes_window = 0;
u8 adjust_hours_window = 0;



void Clock(void)
{

		seconds++;

		if(seconds >= 60)
		{
			seconds = 0;
			minutes++;
		}

		if(minutes >= 60)
		{
			minutes = 0;
			hours++;
		}

		if(hours >= 24)
		{
			hours = 0;
		}

}



void Time_Display(void)
{
	if(seconds_flag)
	{
		SSEGMENT_voidDisplayNum(seconds%10,RIGHT_SEGMENT);
		SSEGMENT_voidDisplayNum(seconds/10,LEFT_SEGMENT);
	}else if(minutes_flag)
	{
		SSEGMENT_voidDisplayNum(minutes%10,RIGHT_SEGMENT);
		SSEGMENT_voidDisplayNum(minutes/10,LEFT_SEGMENT);
	}else if(hours_flag)
	{
		SSEGMENT_voidDisplayNum(hours%10,RIGHT_SEGMENT);
		SSEGMENT_voidDisplayNum(hours/10,LEFT_SEGMENT);
	}
}




void Time_adjust(void)
{
	static u8 temp = 0;

	if(adjust_seconds_flag)
	{
		clock_stop = 1;
		adjust_seconds_window++;
		if(adjust_seconds_window <= 4)
		{
			if(keypad_input_number >= 0)
			{
				adjust_numbers_count++;
				if(adjust_numbers_count == 1)
				{
					if(keypad_input_number <= 5) temp = 10 * keypad_input_number;
				}
				else if(adjust_numbers_count == 2)
				{
					if(keypad_input_number <= 9) temp += keypad_input_number;
					seconds = temp;
					temp = 0;
					adjust_numbers_count = 0;
				}
			}
		}
		else
		{
			clock_stop = 0;
			adjust_numbers_count = 0;
			adjust_seconds_window = 0;
			adjust_seconds_flag = 0;
		}
	}else if(adjust_minutes_flag)
	{
		clock_stop = 1;
		adjust_minutes_window++;
		if(adjust_minutes_window <= 4)
		{
			if(keypad_input_number >= 0)
			{
				adjust_numbers_count++;
				if(adjust_numbers_count == 1)
				{
					if(keypad_input_number <= 5) temp = 10 * keypad_input_number;
				}
				else if(adjust_numbers_count == 2)
				{
					if(keypad_input_number <= 9) temp += keypad_input_number;
					minutes = temp;
					temp = 0;
					adjust_numbers_count = 0;
				}
			}
		}
		else
		{
			clock_stop = 0;
			adjust_numbers_count = 0;
			adjust_minutes_window = 0;
			adjust_minutes_flag = 0;
		}
	}else if(adjust_hours_flag)
	{
		clock_stop = 1;
		adjust_hours_window++;
		if(adjust_hours_window <= 4)
		{
			if(keypad_input_number >= 0)
			{
				adjust_numbers_count++;
				if(adjust_numbers_count == 1)
				{
					if(keypad_input_number <= 2) temp = 10 * keypad_input_number;
				}
				else if(adjust_numbers_count == 2)
				{
					temp += keypad_input_number;
					hours = temp;
					temp = 0;
					adjust_numbers_count = 0;
				}
			}
		}
		else
		{
			clock_stop = 0;
			adjust_numbers_count = 0;
			adjust_hours_window = 0;
			adjust_hours_flag = 0;
		}
	}

}
