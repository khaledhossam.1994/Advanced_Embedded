/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/


#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "DIO.h"
#include "SSEGMENT.h"



static u8 u8Value = 0;


/* Function to display numbers 0-9 on 7 segment display on any 7 segment */
void SSEGMENT_voidDisplayNum(u8 u8Num,u8 segmet_choice)
{

	/*Check if user configured driver for common cathode connection*/
	if(segments[segmet_choice].ss_type == SSEGMENT_COMMON_CATHODE)
	{
		if(segmet_choice < segment_max)
		{
			/*Getting choosen number from u8SegmentValues array*/
			u8Value = u8SegmentValues[u8Num];

			/*Setting corresponding DIO PIN for 7 segment PIN A*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_A,GET_BIT(u8Value,SSEGMENT_PIN_A_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN B*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_B,GET_BIT(u8Value,SSEGMENT_PIN_B_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN C*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_C,GET_BIT(u8Value,SSEGMENT_PIN_C_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN D*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_D,GET_BIT(u8Value,SSEGMENT_PIN_D_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN E*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_E,GET_BIT(u8Value,SSEGMENT_PIN_E_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN F*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_F,GET_BIT(u8Value,SSEGMENT_PIN_F_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN G*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_G,GET_BIT(u8Value,SSEGMENT_PIN_G_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN DOT*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_DOT,GET_BIT(u8Value,SSEGMENT_PIN_DOT_INDEX));
		}
	}


	/* Check if user configured driver for common anode connection*/
	if(segments[segmet_choice].ss_type == SSEGMENT_COMMON_ANODE)
	{
		if(segmet_choice < segment_max)
		{
			/*Getting choosen number from u8SegmentValues array*/
			u8 u8Value = ~u8SegmentValues[u8Num];

			/*Setting corresponding DIO PIN for 7 segment PIN A*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_A,GET_BIT(u8Value,SSEGMENT_PIN_A_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN B*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_B,GET_BIT(u8Value,SSEGMENT_PIN_B_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN C*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_C,GET_BIT(u8Value,SSEGMENT_PIN_C_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN D*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_D,GET_BIT(u8Value,SSEGMENT_PIN_D_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN E*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_E,GET_BIT(u8Value,SSEGMENT_PIN_E_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN F*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_F,GET_BIT(u8Value,SSEGMENT_PIN_F_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN G*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_G,GET_BIT(u8Value,SSEGMENT_PIN_G_INDEX));
			/*Setting corresponding DIO PIN for 7 segment PIN DOT*/
			DIO_voidSetPinValue(segments[segmet_choice].ssegment_pin_DOT,GET_BIT(u8Value,SSEGMENT_PIN_DOT_INDEX));
		}
	}
}

