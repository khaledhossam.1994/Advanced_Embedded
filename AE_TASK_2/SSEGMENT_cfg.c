/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/

#include "STD_TYPES.h"
#include "SSEGMENT_cfg.h"
#include "DIO.h"


/*7 segment values*/
const u8 u8SegmentValues[16] = {0b00111111, // 0
		0b00000110, // 1
		0b01011011, // 2
		0b01001111, // 3
		0b01100110, // 4
		0b01101101, // 5
		0b01111101, // 6
		0b00000111, // 7
		0b01111111, // 8
		0b01101111, // 9
		0b01110111, // A
		0b11111111, // B
		0b00111001, // C
		0b10111111, // D
		0b01111001, // E
		0b01110001}; // F
    
    
const Segment segments[] = {         
       { SSEGMENT_COMMON_ANODE,DIO_u8_PIN0,DIO_u8_PIN1,DIO_u8_PIN2,DIO_u8_PIN3,DIO_u8_PIN4,DIO_u8_PIN5,DIO_u8_PIN6,DIO_u8_PIN7 },
       { SSEGMENT_COMMON_ANODE,DIO_u8_PIN16,DIO_u8_PIN17,DIO_u8_PIN18,DIO_u8_PIN19,DIO_u8_PIN20,DIO_u8_PIN21,DIO_u8_PIN22,DIO_u8_PIN23 }
};


const u8 segment_max = sizeof(segments)/sizeof(Segment);
