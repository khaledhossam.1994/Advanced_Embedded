#include "STD_TYPES.h"
#include "DELAY.h"


void voidDelayMS(u32 ms)
{

	u32 i,u32Count;
	u32Count = ( ms - FUNC_CALL_TIME_MS - LOCAL_EQU_TIME_MS - LOOP_WASTED_TIME_MS - RETURN_TIME_MS)  / (ONE_LOOP_EXEC_MS);

	for(i = 0;i < u32Count;i++)
	{
		asm("NOP");
	}

}


void voidDelayUS(u32 us)
{
	
	u32 i,u32Count;
	u32Count = ( us - FUNC_CALL_TIME_US - LOCAL_EQU_TIME_US - LOOP_WASTED_TIME_US - RETURN_TIME_US)  / (ONE_LOOP_EXEC_US);


	for(i = 0;i < u32Count;i++)
	{
		asm("NOP");
	}

}