/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/


#ifndef _SSEGMENT_INT_H
#define _SSEGMENT_INT_H


#include "SSEGMENT_cfg.h"


/* Function to display numbers 0-9 on 7 segment display */
extern void SSEGMENT_voidDisplayNum(u8 u8Num,u8 segmet_choice);

#endif