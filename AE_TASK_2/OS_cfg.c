/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/


#include "STD_TYPES.h"
#include "OS_cfg.h"
#include "Switch_Led_control.h"
#include "Keypad_handler.h"
#include "Clock.h"
/* Include Tasks header file */





const os_task os_Tasks[] = {
		{Switch_Led_1,100},
		{Switch_Led_2,100},
		{Clock,1000},
		{Time_adjust,500},
		{Time_Display,10},
		{Keypad_Input,10}
};



const u16 tasks_max = sizeof(os_Tasks)/sizeof(os_task);
