/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/

#include "STD_TYPES.h"
#include "LEDS_cfg.h"
#include "DIO.h"


const Led leds[] = {
         {LEDS_u8_MODE_NORMAL,DIO_u8_PIN14,LEDS_u8_STATE_LOW},
         {LEDS_u8_MODE_NORMAL,DIO_u8_PIN15,LEDS_u8_STATE_LOW}
};

const u8 led_max = sizeof(leds)/sizeof(Led);
