/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/

#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "DIO.h"
#include "SWITCH.h"


static u8 u8State = 0;

                                                
/*************************************/
/* Description : This function is    */
/* used to get switch state either   */
/* pushed or released                */
/*************************************/
u8 SWITCH_u8GetSwitchState(u8 u8SwitchIndex)
{
	u8State = SWITCH_RELEASED_STATE;
	if(u8SwitchIndex < switch_max)
	{
		if( DIO_u8GetPinValue(switches[u8SwitchIndex].switch_pin) == switches[u8SwitchIndex].switch_open_state )
		{
			u8State = SWITCH_RELEASED_STATE;
		}
		else
		{
			u8State = SWITCH_PUSHED_STATE;
		}
	}
	else
	{
		/* Error */
		u8State = SWITCH_ERROR_STATE;
	}
	return u8State;
}






