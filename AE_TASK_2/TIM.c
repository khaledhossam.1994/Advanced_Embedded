/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 7 MAR 2018              */
/* Release : VOL  1                  */
/*************************************/
#include "STD_TYPES.h"
#include "BIT_MATH.h"


#include "TIM.h"



/*************************************/
/* Description : This function is    */
/* used to initalize the timer       */
/* peripheral                        */
/*************************************/
extern void TIM_voidInitialize(void)
{


	TIM_voidInitRegister(TIM_u8_COMPARE_REGSITER_VALUE);
	TIM_voidDisableInt();
	SET_BIT(TIFR,1);



	#if TIM_u16_PRESCALER == TIM_u16_NO_DIVISION
		TCCR0 = TIM_u8_PRESCALER_NO_DIV;
	#elif TIM_u16_PRESCALER == TIM_u16_DIV_BY_8
		TCCR0 = TIM_u8_PRESCALER_8;
	#elif TIM_u16_PRESCALER == TIM_u16_DIV_BY_64
		TCCR0 = TIM_u8_PRESCALER_64;
	#elif TIM_u16_PRESCALER == TIM_u16_DIV_BY_256
		TCCR0 = TIM_u8_PRESCALER_256;
	#elif TIM_u16_PRESCALER == TIM_u16_DIV_BY_1024
		TCCR0 = TIM_u8_PRESCALER_1024;
	#endif

}


/*************************************/
/* Description : This function is    */
/* used to enable Timer interrupt    */
/*************************************/
extern void TIM_voidEnableInt(void)
{
	SET_BIT(TIMSK,1);
}


/*************************************/
/* Description : This function is    */
/* used to disable Timer interrupt   */
/*************************************/
extern void TIM_voidDisableInt(void)
{
	CLEAR_BIT(TIMSK,1);
}


/*************************************/
/* Description : This function is    */
/* used to initialize timer register */
/*************************************/
extern void TIM_voidInitRegister(u8 u8ValueCpy)
{
	OCR0 = u8ValueCpy;
}



