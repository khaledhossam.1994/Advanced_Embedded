#ifndef _DELAY_H
#define _DELAY_H
 
#define FUNC_CALL_TIME_MS 0.00354
#define LOCAL_EQU_TIME_MS 0.483
#define ONE_LOOP_EXEC_MS 0.0045
#define LOOP_WASTED_TIME_MS 0.0027
#define RETURN_TIME_MS 0.002

#define FUNC_CALL_TIME_US 3.54
#define LOCAL_EQU_TIME_US 483
#define ONE_LOOP_EXEC_US 4.5
#define LOOP_WASTED_TIME_US 2.7
#define RETURN_TIME_US 2
 
void voidDelayMS(u32 ms);
void voidDelayUS(u32 us);

#endif