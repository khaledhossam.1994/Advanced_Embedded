/*
 * Clock.h
 *
 *  Created on: Mar 7, 2018
 *      Author: admin
 */

#ifndef CLOCK_H_
#define CLOCK_H_


#define RIGHT_SEGMENT 	0
#define LEFT_SEGMENT 	1

extern u8 seconds_flag;
extern u8 minutes_flag;
extern u8 hours_flag;
extern u8 adjust_seconds_flag;
extern u8 adjust_minutes_flag;
extern u8 adjust_hours_flag;
extern u8 adjust_seconds_window;
extern u8 adjust_minutes_window;
extern u8 adjust_hours_window;

extern void Clock(void);
extern void Time_Display(void);
extern void Time_adjust(void);

#endif /* CLOCK_H_ */
