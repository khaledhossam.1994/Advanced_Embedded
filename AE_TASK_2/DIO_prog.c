#include "STD_TYPES.h"
#include "BIT_MATH.h"

#include "DIO_register.h"
#include "DIO_config.h"
#include "DIO_priv.h"
#include "DIO.h"




void DIO_voidInitialize(void)
{
	DDRA = DIO_u8_PORTA_DIRECTION;
	DDRB = DIO_u8_PORTB_DIRECTION;
	DDRC = DIO_u8_PORTC_DIRECTION;
	DDRD = DIO_u8_PORTD_DIRECTION;
}


void DIO_voidSetPinValue(u8 u8PinIndexCpy,u8 u8PinValueCpy)
{
	
	/* check if the PIN index in PORTA range */
	if( (u8PinIndexCpy >= DIO_u8_PORTA_START) && (u8PinIndexCpy <= DIO_u8_PORTA_END) )
	{
		if(u8PinValueCpy == DIO_u8_HIGH)
		{
		    SET_BIT(PORTA,u8PinIndexCpy);
		}
		
		else
		{
			CLEAR_BIT(PORTA,u8PinIndexCpy);
		}
	}
	
	/* check if the PIN index in PORTB range */
	else if( (u8PinIndexCpy >= DIO_u8_PORTB_START) && (u8PinIndexCpy <= DIO_u8_PORTB_END) )
	{
		u8PinIndexCpy -= DIO_u8_PORTA_SIZE;
		
		if(u8PinValueCpy == DIO_u8_HIGH)
		{
		    SET_BIT(PORTB,u8PinIndexCpy);
		}
		
		else
		{
		    CLEAR_BIT(PORTB,u8PinIndexCpy);
		}
	}
	
	
	/* check if the PIN index in PORTC range */
	else if( (u8PinIndexCpy >= DIO_u8_PORTC_START) && (u8PinIndexCpy <= DIO_u8_PORTC_END) )
	{
		u8PinIndexCpy -= (DIO_u8_PORTA_SIZE + DIO_u8_PORTB_SIZE);
		
		if(u8PinValueCpy == DIO_u8_HIGH)
		{
		    SET_BIT(PORTC,u8PinIndexCpy);
		}
		
		else
		{
		    CLEAR_BIT(PORTC,u8PinIndexCpy);
		}
	}
	
	
	/* check if the PIN index in PORTD range */
	else if( (u8PinIndexCpy >= DIO_u8_PORTD_START) && (u8PinIndexCpy <= DIO_u8_PORTD_END) )
	{
		u8PinIndexCpy -= (DIO_u8_PORTA_SIZE + DIO_u8_PORTB_SIZE + DIO_u8_PORTC_SIZE);
		
		if(u8PinValueCpy == DIO_u8_HIGH)
		{
		    SET_BIT(PORTD,u8PinIndexCpy);
		}
		
		else
		{
		    CLEAR_BIT(PORTD,u8PinIndexCpy);
		}
	}
	
	
	
}





u8 DIO_u8GetPinValue(u8 u8PinIndex)
{
	u8 u8Result;
	
	/* check if the PIN index in PORTA range */
	if( (u8PinIndex >= DIO_u8_PORTA_START) && (u8PinIndex <= DIO_u8_PORTA_END) )
	{
		if(GET_BIT(PINA,u8PinIndex))
		{
		    u8Result = DIO_u8_HIGH;
		}
		
		else
		{
			u8Result = DIO_u8_LOW;
		}
	}
	
	/* check if the PIN index in PORTB range */
	else if( (u8PinIndex >= DIO_u8_PORTB_START) && (u8PinIndex <= DIO_u8_PORTB_END) )
	{
		u8PinIndex -= DIO_u8_PORTA_SIZE;
		
		if(GET_BIT(PINB,u8PinIndex))
		{
		    u8Result = DIO_u8_HIGH;
		}
		
		else
		{
		    u8Result = DIO_u8_LOW;
		}
	}
	
	
	/* check if the PIN index in PORTC range */
	else if( (u8PinIndex >= DIO_u8_PORTC_START) && (u8PinIndex <= DIO_u8_PORTC_END) )
	{
		u8PinIndex -= (DIO_u8_PORTA_SIZE + DIO_u8_PORTB_SIZE);
		
		if(GET_BIT(PINC,u8PinIndex))
		{
		    u8Result = DIO_u8_HIGH;
		}
		
		else
		{
		    u8Result = DIO_u8_LOW;
		}
	}
	
	
	/* check if the PIN index in PORTD range */
	else if( (u8PinIndex >= DIO_u8_PORTD_START) && (u8PinIndex <= DIO_u8_PORTD_END) )
	{
		u8PinIndex -= (DIO_u8_PORTA_SIZE + DIO_u8_PORTB_SIZE + DIO_u8_PORTC_SIZE);
		
		if(GET_BIT(PIND,u8PinIndex))
		{
		    u8Result = DIO_u8_HIGH;
		}
		
		else
		{
		    u8Result = DIO_u8_LOW;
		}
	}
	
	return u8Result;

}
