/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/
                                     
#ifndef _SWITCH_CONFIG_H                   
#define _SWITCH_CONFIG_H                   


/* Defining SWITCH states */
#define SWITCH_ERROR_STATE		2
#define SWITCH_PUSHED_STATE		1
#define SWITCH_RELEASED_STATE	0


typedef struct{
  u8 switch_pin;
  u8 switch_open_state;
}Switch;

extern const Switch switches[];
extern const u8 switch_max; 

#endif
