/*************************************/
/* Author  : Khaled Hossam           */
/* Date    : 6 MAR  2018             */
/* Release : VOL  1                  */
/*************************************/
                                     
#ifndef _KEYPAD_CONFIG_H                   
#define _KEYPAD_CONFIG_H                   

#define KEYPAD_u8_INP_1 DIO_u8_PIN28
#define KEYPAD_u8_INP_2 DIO_u8_PIN29
#define KEYPAD_u8_INP_3 DIO_u8_PIN30
#define KEYPAD_u8_INP_4 DIO_u8_PIN31

#define KEYPAD_u8_OUT_1 DIO_u8_PIN24
#define KEYPAD_u8_OUT_2 DIO_u8_PIN25
#define KEYPAD_u8_OUT_3 DIO_u8_PIN26
#define KEYPAD_u8_OUT_4 DIO_u8_PIN27

#endif
