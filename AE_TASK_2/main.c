/*
 * main.c
 *
 *  Created on: Mar 6, 2018
 *      Author: admin
 */

#include "STD_TYPES.h"
#include "BIT_MATH.h"
#include "DELAY.h"
#include "DIO.h"
#include "DIO_register.h"
#include "LEDS.h"
#include "SWITCH.h"
#include "SSEGMENT.h"
#include "KEYPAD.h"
#include "TIM.h"
#include "GIE_int.h"
#include "OS.h"



void main(void)
{

	DIO_voidInitialize();
	LEDS_voidInitialize();

	DIO_voidSetPinValue(DIO_u8_PIN8,DIO_u8_HIGH);
	DIO_voidSetPinValue(DIO_u8_PIN9,DIO_u8_HIGH);

	TIM_voidInitialize();
	OS_init();

	TIM_voidEnableInt();
	GIE_voidEnableGlobalInterrupt();

	OS_start();

}
